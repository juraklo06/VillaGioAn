window.addEventListener( "DOMContentLoaded", () => Array.from( document.querySelectorAll('.kn-gioan-gallery-images-outer-container')).forEach( (item) => {

    const arrLeft = item.querySelector('.kn-gioan-gallery-images-control .kn-gioan-slider-previous');
    const arrRight = item.querySelector('.kn-gioan-gallery-images-control .kn-gioan-slider-next');
    
    const images = Array.from( item.querySelectorAll( ".kn-gioan-gallery-images-container .kn-gioan-gallery-image"));

    var id = 0;
    var lastId = 0;

    var transitioning = false;

    images.forEach( image => image.addEventListener( "transitionend", ( event) =>{

        if( event.propertyName === "opacity"){

            images[ lastId].classList.remove('kn-gioan-image-fade');
            images[ lastId].classList.remove('kn-gioan-active');
            images[id].classList.add('kn-gioan-active');
            transitioning = false;
        }

    }));

    function slide(id){
        images[  lastId].classList.add('kn-gioan-image-fade');
    }

    arrLeft.addEventListener('click',() => {
        
        if( !transitioning)
            transitioning = true;
        else
            return;

        lastId = id;
        id--;
        if(id < 0){
            id = images.length - 1;
        }        slide(id);
    })

    arrRight.addEventListener('click',() => {

        if( !transitioning)
            transitioning = true;
        else
            return;
            
        lastId = id;
        id++;
        if(id > images.length - 1){
            id=0;
        }  
        slide(id);
    })

}));


