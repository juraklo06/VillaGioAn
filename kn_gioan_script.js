jQuery(document).ready( function(){

    jQuery( ".kn-gioan-slider-gallery-item-delete-button").click( ( event) =>{
        
        event.preventDefault();

        let data = {
            action: 'OnDeleteKNGioanSliderImage',
            security: kn_gioan_script.ajaxnonce,
            id: event.target.dataset.id,
       };
             
       jQuery.post( 
            kn_gioan_script.ajaxurl, 
           data,                   
           function( response ) {
               window.location.reload();
           }
       );
    });
});