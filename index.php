<?php

/*
 * Plugin Name:       Gioan slider plugin
 * Plugin URI:        https://koona.tech
 * Description:       Plugin which allows adding multiple sliders on page for customization
 * Version:           1.0.0
 * Requires at least: 5.2
 * Requires PHP:      7.2
 * Author:            Jure Babić
 * Author URI:        https://koona.tech
 * License:           GPL v2 or later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Update URI:        https://example.com/my-plugin/
 * Text Domain:       kn-gioan-slider-plugin
 * Domain Path:       /languages
 */


 global $KN_gioan_db_version;
 $KN_gioan_db_version = '1.0';
 


function knGioanSliderPluginActivation(){
    
    global $wpdb;
    global $KN_gioan_db_version;

    $table_name = $wpdb->prefix . 'wp_kn_gioan_slider_gallery';
    
    $charset_collate = $wpdb->get_charset_collate();

    $sql = "CREATE TABLE $table_name (
        id INT NOT NULL AUTO_INCREMENT,
        galleryId INT NOT NULL,
        src VARCHAR(2048) DEFAULT '' NOT NULL,
        alt VARCHAR(2048) DEFAULT '' NOT NULL,
        PRIMARY KEY  (id)
    ) $charset_collate;";

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

    add_option( 'KN_gioan_db_version', $KN_gioan_db_version );
    
    dbDelta( $sql );

    flush_rewrite_rules(); 
}

 register_activation_hook(
	__FILE__,
	'knGioanSliderPluginActivation'
);

function knGioanSliderFunction( $atts ) {

    global $wpdb;

	$attributes = shortcode_atts( array(
		'galleryid' => null,
	), $atts );

    $results = $wpdb -> get_results( 'SELECT * FROM `'. $wpdb->prefix .'wp_kn_gioan_slider_gallery` WHERE galleryId = ' . $attributes['galleryid']);

    $resHTML = "<div class='kn-gioan-gallery-images-outer-container'><div class='kn-gioan-gallery-images-container'>";

    foreach( $results as $index => $value){

        $resHTML .= "<img class='kn-gioan-gallery-image " . ( $index === 0 ? "kn-gioan-active" : ""). "' src='". $value -> src ."'/>";
    }


	return $resHTML . "</div><div class='kn-gioan-gallery-images-control'><button class='kn-gioan-slider-previous'>Prev</button><button class='kn-gioan-slider-next'>Next</button></div></div>";
}
add_shortcode( 'kn_gioan_slider', 'knGioanSliderFunction' );


function kn_gioan_enqueue( $hook) {

	wp_enqueue_script(
        'kn_gioan_script',
        plugins_url( '/js/kn_gioan_script.js', __FILE__ ),
        array( "jquery" ),
        '1.0.0',
        true
    );
	wp_enqueue_style(
		'kn_gioan_style',
		plugins_url( '/css/kn_gioan_style.css', __FILE__),
		array(),
		'1.0'
	);

    wp_localize_script( 
        'kn_gioan_script', 
        'kn_gioan_script',
        array( 
            'ajaxurl'     => admin_url( 'admin-ajax.php' ),
            'ajaxnonce'   => wp_create_nonce( 'my_ajax_validation' ) // <--- Security!
        ) 
    );
}
add_action( 'admin_enqueue_scripts', 'kn_gioan_enqueue' );


function kn_gioan_enqueue_frontend( $hook) {

	wp_enqueue_script(
        'kn_gioan_script_frontend',
        plugins_url( '/js/kn_gioan_script_frontend.js', __FILE__ ),
        array( "jquery" ),
        '1.0.0',
        true
    );
	wp_enqueue_style(
		'kn_gioan_style',
		plugins_url( '/css/kn_gioan_style_frontend.css', __FILE__),
		array(),
		'1.1'
	);

}
add_action( 'wp_enqueue_scripts', 'kn_gioan_enqueue_frontend' );


function OnDeleteKNGioanSliderImage(){

    global $wpdb;

    check_ajax_referer( 'my_ajax_validation', 'security' );
    
    $imageId = $_POST[ "id"];

    $wpdb -> delete( $wpdb->prefix .'wp_kn_gioan_slider_gallery', array( 'id' => $imageId ), array( '%d' ) );

	wp_send_json_success( array( ' success' => __( 'Success')) );
}

add_action('wp_ajax_OnDeleteKNGioanSliderImage', 'OnDeleteKNGioanSliderImage');
add_action('wp_ajax_no_priv_OnDeleteKNGioanSliderImage', 'OnDeleteKNGioanSliderImage');

function knGioanSliderGalleryForm(){

    global $wpdb;

    $results = $wpdb -> get_results( 'SELECT * FROM `'. $wpdb->prefix .'wp_kn_gioan_slider_gallery` ORDER BY galleryId ASC');

    ?>
    <div class="kn-gioan-slider-options-page">
        <div class="kn-gioan-slider-galleries">
            
        <?php if( count($results) > 0):
            
            $galleryId = $results[0] -> galleryId;?>
            <div class="kn-gioan-slider-gallery">
                <h4 class="kn-gioan-slider-gallery-title">Gallery ID: <?php echo $galleryId;?></h4>
        <?php endif; 
        
        foreach( $results as $index => $result): 
            if( $result -> galleryId !== $galleryId):
                $galleryId = $result -> galleryId;
                ?>
                </div>
                <div class="kn-gioan-slider-gallery">
                    <h4 class="kn-gioan-slider-gallery-title">Gallery id: <?php echo $galleryId;?></h4>
        <?php endif;?>

            <div class="kn-gioan-slider-gallery-item">
                <img src="<?php echo $result -> src;?>" alt="<?php echo $result -> alt; ?>" />
                <button class="kn-gioan-slider-gallery-item-delete-button" data-id="<?php echo $result -> id;?>">X</button>
            </div>
        <?php endforeach;?>
        <?php if( count($results) > 0):?>
            </div>
        <?php endif; ?>
        </div>
        <div class="kn-gioan-gallery-form">
            <p>Add to new gallery (<i> If you want to add to existing, just change gallery ID number</i>)</p>
            <label>Select images to add to gallery</p>
            <input name="galleryImages[]" type="file" multiple accept="image/*">
            <label>Gallery ID</label>
            <input name="galleryId" type="number" value="<?php echo ($galleryId + 1);?>" />
            <button type="submit" class="kn-gioan-submit-button">Add gallery images</button>
        </div>
    </div>
<?php
};

add_action("edit_form_after_editor", "knGioanSliderGalleryForm");

function knGioanSliderGalleryAdd( $postId){
    
    global $wpdb;

    if( isset($_FILES['galleryImages']) && ! empty($_FILES['galleryImages']) ) {
        
        $saveImages = [];

        if( is_array($_FILES['galleryImages']['name'])){

            for( $i = 0; $i < count( $_FILES['galleryImages']['name']); $i++){
                
                $upload       = wp_upload_bits( $_FILES['galleryImages']['name'][$i], null, file_get_contents( $_FILES['galleryImages']['tmp_name'][$i] ) );
                $filetype     = wp_check_filetype( basename( $upload['file'] ), null );
                $upload_dir   = wp_upload_dir();
                $upl_base_url = is_ssl() ? str_replace('http://', 'https://', $upload_dir['baseurl']) : $upload_dir['baseurl'];
                $base_name    = basename( $upload['file'] );

                $saveImages[] = array(
                    'galleryId' => $_POST["galleryId"],
                    'src'      => "'".$upl_base_url .'/'. _wp_relative_upload_path( $upload['file'] ). "'", 
                    'alt'     => "'".ucfirst( preg_replace('/\.[^.]+$/', '', $base_name ) ). "'",
                );
            }
        }else{
            $upload       = wp_upload_bits( $_FILES['galleryImages']['name'], null, file_get_contents( $_FILES['galleryImages']['tmp_name'] ) );
            $filetype     = wp_check_filetype( basename( $upload['file'] ), null );
            $upload_dir   = wp_upload_dir();
            $upl_base_url = is_ssl() ? str_replace('http://', 'https://', $upload_dir['baseurl']) : $upload_dir['baseurl'];
            $base_name    = basename( $upload['file'] );

            $saveImages[] = array(
                'galleryId' => $_POST["galleryId"],
                'src'      => "'".$upl_base_url .'/'. _wp_relative_upload_path( $upload['file'] ). "'", 
                'alt'     => "'".ucfirst( preg_replace('/\.[^.]+$/', '', $base_name ) ). "'",
            );
        }

        $sqlValues = [];

        foreach( $saveImages as $saveImg){

            $sqlValues[] = "(" . implode(",", $saveImg) . ")";
        }

        $sql = "INSERT INTO `" . $wpdb->prefix ."wp_kn_gioan_slider_gallery` (  galleryId, src, alt) VALUES " . implode( ",", $sqlValues);

        $wpdb -> query($wpdb->prepare( $sql));

   }

}

add_action( "save_post", "knGioanSliderGalleryAdd");

add_action('post_edit_form_tag', 'add_post_enctype');

function add_post_enctype() {
    echo ' enctype="multipart/form-data"';
}
?>